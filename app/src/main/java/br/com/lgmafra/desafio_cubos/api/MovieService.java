package br.com.lgmafra.desafio_cubos.api;

import java.util.List;

import br.com.lgmafra.desafio_cubos.model.MovieDetail;
import br.com.lgmafra.desafio_cubos.model.SearchResult;
import br.com.lgmafra.desafio_cubos.util.Constants;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieService {
    @GET(Constants.API_URL_LIST_MOVIES_PARAMS)
    Call<SearchResult> listMovies(
            @Query(value = Constants.API_URL_QUERY_API_KEY) String api_key,
            @Query(value = Constants.API_URL_QUERY_GENRES) int genre_id,
            @Query(value = Constants.API_URL_QUERY_LANGUAGE) String language,
            @Query(value = Constants.API_URL_QUERY_SORT) String sort_by,
            @Query(value = Constants.API_URL_QUERY_PAGE) int page
    );

    @GET(Constants.API_URL_MOVIE_DETAIL)
    Call<MovieDetail> movieDetail(
            @Path(Constants.API_URL_QUERY_MOVIE_ID) int movie_id,
            @Query(value = Constants.API_URL_QUERY_API_KEY) String api_key,
            @Query(value = Constants.API_URL_QUERY_LANGUAGE) String language
    );

    @GET(Constants.API_URL_MOVIE_SEARCH)
    Call<SearchResult> movieSearch(
            @Query(value = Constants.API_URL_QUERY_QUERY) String query,
            @Query(value = Constants.API_URL_QUERY_API_KEY) String api_key,
            @Query(value = Constants.API_URL_QUERY_LANGUAGE) String language,
            @Query(value = Constants.API_URL_QUERY_PAGE) int page
    );
}
