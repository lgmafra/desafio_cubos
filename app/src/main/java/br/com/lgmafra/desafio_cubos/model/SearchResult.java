package br.com.lgmafra.desafio_cubos.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResult{

    @SerializedName("page")
    public int page;
    @SerializedName("total_pages")
    public int total_pages;
    @SerializedName("results")
    public List<Movies> movies;
}
