package br.com.lgmafra.desafio_cubos.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import br.com.lgmafra.desafio_cubos.model.MovieDetail;
import br.com.lgmafra.desafio_cubos.repository.MovieRepository;

public class MovieDetailViewModel extends ViewModel {

    private MovieRepository repository;
    private LiveData<MovieDetail> movies;

    public MovieDetailViewModel(MovieRepository repository){
        this.repository = repository;
    }

    public void init(int movie_id){
        if(this.movies != null){
            return;
        }

        this.movies = this.repository.getMovieDetail(movie_id);
    }

    public LiveData<MovieDetail> getMovies(){
        return this.movies;
    }

}
