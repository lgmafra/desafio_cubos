package br.com.lgmafra.desafio_cubos.model;

import com.google.gson.annotations.SerializedName;

public class Movies{
    @SerializedName("id")
    public int movie_id;
    @SerializedName("poster_path")
    public String poster_path;
    @SerializedName("title")
    public String title;
}
