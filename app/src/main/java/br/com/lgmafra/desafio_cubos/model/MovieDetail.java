package br.com.lgmafra.desafio_cubos.model;

import com.google.gson.annotations.SerializedName;

public class MovieDetail {

    @SerializedName("title")
    public String title;
    @SerializedName("poster_path")
    public String poster_path;
    @SerializedName("overview")
    public String overview;
}
