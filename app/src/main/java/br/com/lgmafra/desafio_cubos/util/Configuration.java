package br.com.lgmafra.desafio_cubos.util;

import android.content.Context;
import android.net.ConnectivityManager;

public class Configuration {

    /* Função para verificar existência de conexão com a internet
     */
    public static boolean checkInternetConnection(Context context) {
        boolean connected;

        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            connected = true;
        } else {
            connected = false;
        }
        return connected;
    }
}
