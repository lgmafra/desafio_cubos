package br.com.lgmafra.desafio_cubos.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import br.com.lgmafra.desafio_cubos.api.APIConfiguration;
import br.com.lgmafra.desafio_cubos.model.MovieDetail;
import br.com.lgmafra.desafio_cubos.model.Movies;
import br.com.lgmafra.desafio_cubos.model.SearchResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRepository {

    public LiveData<SearchResult> getMovies(int genre_id, int page){
        MutableLiveData<SearchResult> data = new MutableLiveData<>();

        new APIConfiguration().getListMovie(genre_id, page).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {

            }
        });

        return data;
    }

    public LiveData<SearchResult> getMoviesSearch(String query, int page){
        final MutableLiveData<SearchResult> data = new MutableLiveData<>();

        new APIConfiguration().getMovieSearch(query, page).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {

            }
        });

        return data;
    }

    public LiveData<MovieDetail> getMovieDetail(int movie_id){
        final MutableLiveData<MovieDetail> data = new MutableLiveData<>();

        new APIConfiguration().getMovieDetail(movie_id).enqueue(new Callback<MovieDetail>() {
            @Override
            public void onResponse(Call<MovieDetail> call, Response<MovieDetail> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<MovieDetail> call, Throwable t) {

            }
        });

        return data;
    }
}
