package br.com.lgmafra.desafio_cubos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.lgmafra.desafio_cubos.MainActivity;
import br.com.lgmafra.desafio_cubos.MovieFindActivity;
import br.com.lgmafra.desafio_cubos.R;
import br.com.lgmafra.desafio_cubos.model.Movies;
import br.com.lgmafra.desafio_cubos.util.Constants;


public class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.ViewHolder> {

    private List<Movies> mValues;
    private Context mContext = null;
    private MainActivity mainActivity;
    private MovieFindActivity movieFindActivity;

    public void setMoviesList(List<Movies> items){
        mValues = items;
        notifyDataSetChanged();
    }

    public MovieRecyclerViewAdapter(MainActivity activity) {
        mainActivity = activity;
    }

    public MovieRecyclerViewAdapter(MovieFindActivity activity) {
        movieFindActivity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_movie, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mMovieId.setText(String.valueOf(mValues.get(position).movie_id));
        holder.mMovieName.setText(mValues.get(position).title);
        Picasso.get()
                .load(Constants.API_URL_IMAGE_POSTER +mValues.get(position).poster_path)
                .into(holder.mMovieImage);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mainActivity != null){
                    mainActivity.showMovieDetail(Integer.parseInt(holder.mMovieId.getText().toString()));
                }else if (movieFindActivity != null){
                    movieFindActivity.showMovieDetail(Integer.parseInt(holder.mMovieId.getText().toString()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues != null ? mValues.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mMovieId, mMovieName;
        public final ImageView mMovieImage;
        public Movies mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMovieId = view.findViewById(R.id.movie_id);
            mMovieName = view.findViewById(R.id.movie_name);
            mMovieImage = view.findViewById(R.id.movie_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mMovieName.getText() + "'";
        }
    }
}
