package br.com.lgmafra.desafio_cubos;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import br.com.lgmafra.desafio_cubos.fragments.SearchMovieFragment;
import br.com.lgmafra.desafio_cubos.repository.MovieRepository;
import br.com.lgmafra.desafio_cubos.util.Configuration;
import br.com.lgmafra.desafio_cubos.util.Constants;
import br.com.lgmafra.desafio_cubos.viewmodel.MovieViewModel;;

public class MovieFindActivity extends AppCompatActivity {

    private MovieViewModel viewModel = new MovieViewModel(new MovieRepository());
    private int page = 1;
    private static final String CONTENT_VIEW_ID = "movies_fragment";
    private ProgressBar p;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_find);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        p = findViewById(R.id.pb_movies);

        if(!Configuration.checkInternetConnection(getApplicationContext())){
            Toast.makeText(
                    getApplicationContext(),
                    "Sem conexão com a internet.",
                    Toast.LENGTH_SHORT
            ).show();
            return;
        }

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            setFragment(query);
        }
    }

    public void showMovieDetail(int movie_id){
        startActivity(new Intent(this, MovieDetailActivity.class).putExtra(Constants.ACTIVITY_PARAM, movie_id));
    }

    public void setFragment(String query){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(CONTENT_VIEW_ID);
        if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            fragment = SearchMovieFragment.newInstance(query, p, MovieFindActivity.this);
            ft.add(R.id.fragment_movie, fragment, CONTENT_VIEW_ID);
            ft.commit();
        }
    }
}
