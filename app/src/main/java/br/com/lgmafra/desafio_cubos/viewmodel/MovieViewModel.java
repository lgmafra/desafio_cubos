package br.com.lgmafra.desafio_cubos.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import br.com.lgmafra.desafio_cubos.model.Movies;
import br.com.lgmafra.desafio_cubos.model.SearchResult;
import br.com.lgmafra.desafio_cubos.repository.MovieRepository;

public class MovieViewModel extends ViewModel {

    private MovieRepository repository;
    private LiveData<SearchResult> movies;

    public MovieViewModel(MovieRepository repository){
        this.repository = repository;
    }

    /**
     * Init the ViewModel with genre_id and page params
     */
    public void init(int genre_id, int page){
        this.movies = this.repository.getMovies(genre_id, page);
    }

    /**
     * Init the ViewModel with query and page params
     */
    public void init(String query, int page){
        this.movies = this.repository.getMoviesSearch(query, page);
    }

    public LiveData<SearchResult> getMovies(){
        return this.movies;
    }
}
