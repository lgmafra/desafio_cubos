package br.com.lgmafra.desafio_cubos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import br.com.lgmafra.desafio_cubos.repository.MovieRepository;
import br.com.lgmafra.desafio_cubos.util.Configuration;
import br.com.lgmafra.desafio_cubos.util.Constants;
import br.com.lgmafra.desafio_cubos.viewmodel.MovieDetailViewModel;

public class MovieDetailActivity extends AppCompatActivity {

    private MovieDetailViewModel viewModel = new MovieDetailViewModel(new MovieRepository());
    private TextView overview;
    private ImageView poster;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeComponents();

        if(!Configuration.checkInternetConnection(getApplicationContext())){
            Toast.makeText(
                    getApplicationContext(),
                    "Sem conexão com a internet.",
                    Toast.LENGTH_SHORT
            ).show();

            return;
        }

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            int movie_id = bundle.getInt(Constants.ACTIVITY_PARAM);

            progressBar.setVisibility(View.VISIBLE);

            viewModel.init(movie_id);
            viewModel.getMovies().observe(this, movie -> {
                toolbar.setTitle(movie.title);
                overview.setText(movie.overview);
                Picasso.get()
                        .load(Constants.API_URL_IMAGE_POSTER_ORIGINAL + movie.poster_path)
                        .into(poster, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                Toast.makeText(
                                    getApplicationContext(),
                                    "Erro ao carregar a imagem.",
                                    Toast.LENGTH_SHORT
                                ).show();
                            }
                        });
            });
        }
    }

    public void initializeComponents(){
        poster = findViewById(R.id.movie_image);
        overview = findViewById(R.id.overview);
        progressBar = findViewById(R.id.pb_movies);
    }

}
