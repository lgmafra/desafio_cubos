package br.com.lgmafra.desafio_cubos.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.lgmafra.desafio_cubos.MainActivity;
import br.com.lgmafra.desafio_cubos.adapter.MovieRecyclerViewAdapter;
import br.com.lgmafra.desafio_cubos.R;
import br.com.lgmafra.desafio_cubos.model.Movies;
import br.com.lgmafra.desafio_cubos.repository.MovieRepository;
import br.com.lgmafra.desafio_cubos.util.Constants;
import br.com.lgmafra.desafio_cubos.util.EndlessRecyclerOnScrollListener;
import br.com.lgmafra.desafio_cubos.viewmodel.MovieViewModel;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ActionMovieFragment extends Fragment{

    private static List<Movies> moviesList = new ArrayList<>();
    // TODO: Customize parameters
    private int mColumnCount = 2;
    private OnListFragmentInteractionListener mListener;
    private MovieViewModel viewModel = new MovieViewModel(new MovieRepository());
    private View view;
    private Context context;
    private static MainActivity mainActivity;
    private static ProgressBar progressBar;
    private MovieRecyclerViewAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ActionMovieFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ActionMovieFragment newInstance(MainActivity activity, ProgressBar p) {
        ActionMovieFragment fragment = new ActionMovieFragment();

        mainActivity = activity;
        progressBar = p;

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressBar.setVisibility(View.VISIBLE);

        viewModel.init(Constants.API_GENRE_ACTION_ID, 1);
        viewModel.getMovies().observe(this, movies -> {
            moviesList.addAll(movies.movies);

            adapter.setMoviesList(moviesList);

            progressBar.setVisibility(View.GONE);
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view;
        GridLayoutManager grid = new GridLayoutManager(context, mColumnCount);

        recyclerView.setLayoutManager(grid);
        adapter = new MovieRecyclerViewAdapter(mainActivity);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int page) {
                progressBar.setVisibility(View.VISIBLE);

                viewModel.init(Constants.API_GENRE_ACTION_ID, page);
                viewModel.getMovies().observe(ActionMovieFragment.this, movies -> {
                    moviesList.addAll(movies.movies);

                    adapter.setMoviesList(moviesList);

                    progressBar.setVisibility(View.GONE);
                });
            }
        });

        // Set the adapter
        if (view instanceof RecyclerView) {
            context = view.getContext();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            //throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Movies item);
    }
}
