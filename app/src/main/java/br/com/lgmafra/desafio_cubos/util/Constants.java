package br.com.lgmafra.desafio_cubos.util;

public class Constants {
    public static final String API_TOKEN = "b835f7bcdb8be24a9f92c5fba54c15c8";
    public static final String API_URL = "https://api.themoviedb.org/3/";
    public static final String API_URL_LIST_MOVIES_PARAMS = "discover/movie";
    public static final String API_URL_MOVIE_DETAIL = "movie/{movie_id}";
    public static final String API_URL_MOVIE_SEARCH = "search/movie";

    public static final String API_URL_QUERY_API_KEY = "api_key";
    public static final String API_URL_QUERY_GENRES = "with_genres";
    public static final String API_URL_QUERY_LANGUAGE = "language";
    public static final String API_URL_QUERY_MOVIE_ID = "movie_id";
    public static final String API_URL_QUERY_SORT = "sort_by";
    public static final String API_URL_QUERY_PAGE = "page";
    public static final String API_URL_QUERY_QUERY = "query";

    public static final String API_URL_IMAGE_POSTER = "https://image.tmdb.org/t/p/w342";
    public static final String API_URL_IMAGE_POSTER_ORIGINAL = "https://image.tmdb.org/t/p/original";

    public static final String API_LANGUAGE = "pt-BR";
    public static final String API_SORT_BY = "popularity.desc";

    public static final String ACTIVITY_PARAM = "movie_id";
    public static final String ACTIVITY_PARAM_QUERY = "query_movie";

    public static final int API_GENRE_ACTION_ID = 28;
    public static final int API_GENRE_DRAMA_ID = 18;
    public static final int API_GENRE_FANTASY_ID = 14;
    public static final int API_GENRE_FICTION_ID = 878;

}
