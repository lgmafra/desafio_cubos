package br.com.lgmafra.desafio_cubos.api;

import br.com.lgmafra.desafio_cubos.model.MovieDetail;
import br.com.lgmafra.desafio_cubos.model.SearchResult;
import br.com.lgmafra.desafio_cubos.util.Constants;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIConfiguration {

    private final Retrofit retrofit;

    public APIConfiguration(){
        this.retrofit =  new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private MovieService getMovieData(){
        return this.retrofit.create(MovieService.class);
    }

    public Call<SearchResult> getListMovie(int genre_id, int page){
        return getMovieData().listMovies(
                Constants.API_TOKEN,
                genre_id,
                Constants.API_LANGUAGE,
                Constants.API_SORT_BY,
                page
        );
    }

    public Call<MovieDetail> getMovieDetail(int movie_id){
        return getMovieData().movieDetail(
                movie_id,
                Constants.API_TOKEN,
                Constants.API_LANGUAGE
        );
    }

    public Call<SearchResult> getMovieSearch(String query, int page){
        return getMovieData().movieSearch(
                query,
                Constants.API_TOKEN,
                Constants.API_LANGUAGE,
                page
        );
    }
}
